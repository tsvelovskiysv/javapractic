package main.java.ru.tsvelovskiy.module00.Unit6;

import java.util.Scanner;

public class Game {
    int kucha;
    int shag;
    int ochered;
    Game() {
        this.game();
    }
    private void game(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите размер кучи: ");
        this.kucha = sc.nextInt();
        System.out.print("Введите размер шага: ");
        this.shag = sc.nextInt();
        int counter = 0;
        int playerShag=1;
        int computerShag=1;
        int ocheredSwitch=0;

        System.out.print("Введите кто ходит первый. 0 - игрок, 1 - компьютер: ");
        this.ochered = sc.nextInt();
        while (kucha>0) {
            counter++;
            ocheredSwitch = (this.ochered % 2 == 0) ? 0 : 1;
            switch (ocheredSwitch) {
                case (0):
                    System.out.print("Введите сколько забрать: ");
                    playerShag = sc.nextInt();
                    System.out.printf("%d ход: ", counter);
                    shag(this.kucha, playerShag, "Игрок");
                    this.ochered++;
                    break;
                default:
                    System.out.printf("%d ход: ", counter);
                    for (int i = 1; i <= this.shag; i++) {
                        if ((this.kucha - i) % (this.shag + 1) == 0) {
                            computerShag = i;
                            break;
                        } else {
                            computerShag = 1;
                        }
                    }
                    shag(this.kucha, computerShag, "Компьютер");
                    this.ochered++;
                    break;
            }
        }

    }
    private void shag(int kucha, int shag, String player){
        this.kucha-=shag;
        System.out.printf("%s отнимает %d из кучи. Осталось в куче: %d\n", player, shag, this.kucha);
        if (this.kucha<1) {
            System.out.printf("\nПобедил %s", player);
        }
    }
}
