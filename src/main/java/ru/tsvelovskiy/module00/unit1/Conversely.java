package main.java.ru.tsvelovskiy.module00.unit1;
import java.util.Scanner;

class Conversely {

    Conversely(){
        this.conversely();
    }

    private void conversely() {
        Scanner sc = new Scanner(System.in);
        int count;
        String numberString;
        System.out.print("Введите целое число: ");
        do {
            numberString = sc.nextLine();
        } while (!scan(numberString));
        int numberInt = Integer.parseInt(numberString);
        StringBuilder sb = new StringBuilder();
        sb.append(numberString);
        if (numberInt >= 0)  {
            sb.reverse();
        } else {
            sb.append("-");
            sb.reverse();
            sb.deleteCharAt(numberString.length());
        }
        String converselyNumberString = sb.toString();

        int converselyNumber = Integer.parseInt(converselyNumberString);
        System.out.printf("Число наоборот: %d", converselyNumber);
        sc.close();
    }

    private boolean scan(String str) {
        try
        {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException ex)
        {
            System.out.println("Ошибка, введено не целое число. Введите число повторно: ");
            return false;
        }

    }
}
