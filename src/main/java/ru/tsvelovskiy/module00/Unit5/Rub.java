package main.java.ru.tsvelovskiy.module00.Unit5;

import java.util.Scanner;

public class Rub {
    Rub(){
        this.rub();
    }

    public static void rub() {
        System.out.print("Введите количество рублей от 0 до 1000: ");
        Scanner sc = new Scanner(System.in);
        int rub = sc.nextInt();
        int rubLastOne = rub%10;
        int rubLastTwo = rub%100;

        String currency = "";

        if (rubLastTwo >=11 && rubLastTwo<=14){
            currency = "рублей";
        } else {
            switch (rubLastOne) {
                default:
                    currency = "рублей";
                    break;
                case '1':
                    currency = "рубль";
                    break;
                case '2':
                case '3':
                case '4':
                    currency = "рубля";
                    break;
            }
        }
        System.out.printf("%d %s",rub,currency);
        sc.close();
    }

}
