package main.java.ru.tsvelovskiy.module00.unit3;

import java.util.Scanner;

class Season {
    enum Seasons {ЗИМА, ОСЕНЬ, ЛЕТО, ВЕСНА}
    int month;

    Season() {
        this.season();
    }

    private void season() {

        Scanner sc = new Scanner(System.in);
        String month;
        System.out.print("Введите номер месяца: ");
        do {
            month = sc.nextLine();
        } while (!scan(month));

        switch (month) {
            case "12":
            case "1":
            case "2":
                System.out.printf("Этот месяц соответствует сезону: %s", Seasons.ЗИМА);
            case "3":
            case "4":
            case "5":
                System.out.printf("Этот месяц соответствует сезону: %s", Seasons.ВЕСНА);
            case "6":
            case "7":
            case "8":
                System.out.printf("Этот месяц соответствует сезону: %s", Seasons.ЛЕТО);
            case "9":
            case "10":
            case "11":
                System.out.printf("Этот месяц соответствует сезону: %s", Seasons.ОСЕНЬ);
        }
        sc.close();
    }

    private boolean scan(String str) {
        try
        {
            if (Integer.parseInt(str)>0 && Integer.parseInt(str)<=12) {
            return true;
            }
            else {
                System.out.println("Ошибка. Введите число повторно: ");
                return false;
            }
        } catch (NumberFormatException ex)
        {
            System.out.println("Ошибка. Введите число повторно: ");
            return false;
        }

    }
}
