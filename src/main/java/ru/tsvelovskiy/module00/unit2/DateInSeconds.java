package main.java.ru.tsvelovskiy.module00.unit2;

import java.util.Scanner;

public class DateInSeconds {
    int minutes = 0;
    int seconds = 0;
    int hours = 0;
    DateInSeconds(){
        this.dateInSeconds();
    }

    private void dateInSeconds() {
        Scanner sc = new Scanner(System.in);
        String time;
        System.out.print("Введите время в формате чч:мм:сс : ");
        time = sc.nextLine();
        while (!scan(time)) {
            System.out.print("Ошибка формата. Введите время в формате чч:мм:сс : ");
            time = sc.nextLine();
        }
        int seconds = this.hours*360+this.minutes*60+this.seconds;
        System.out.printf("Время в секундах: %d\n", seconds);
        sc.close();
    }

    private boolean scan(String str) {
        StringBuilder hh= new StringBuilder();
        StringBuilder mm = new StringBuilder();
        StringBuilder ss = new StringBuilder();
        int hours,minutes,seconds;
        if ((str.length()==8) && (str.charAt(2)==':') && (str.charAt(5)==':')) {
            try {
                hh.append(str.charAt(0)).append(str.charAt(1));
                mm.append(str.charAt(3)).append(str.charAt(4));
                ss.append(str.charAt(6)).append(str.charAt(7));
                hours = Integer.parseInt(hh.toString());
                minutes = Integer.parseInt(mm.toString());
                seconds = Integer.parseInt(ss.toString());
                if (hours <= 24 && hours >= 0 && minutes < 60 && minutes >= 0 && seconds < 60 && seconds >= 0) {
                    this.hours = hours;
                    this.minutes = minutes;
                    this.seconds = seconds;
                    return true;
                } else {
                    return false;
                }
            } catch (NumberFormatException ex) {

                return false;
            }
        } else {
            return false;
        }

    }
}
