package main.java.ru.tsvelovskiy.module00.unit4;

import java.util.Scanner;

public class NextDate {
    int day, month, year;

    NextDate(){
        this.nextDate();
    }
    private void nextDate () {
        Scanner sc = new Scanner(System.in);
        String date;
        System.out.print("Введите дату в формате дд.мм.гггг : ");
        date = sc.nextLine();
        while (!scan(date)) {
            System.out.print("Ошибка формата. Введите время в формате дд.мм.гггг : ");
            date = sc.nextLine();
        }
        switch (this.month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 10:
                if (this.day<31){
                    day++;
                } else {
                    day=1;
                    this.month++;
                }
                break;
            case 2:
                if (leapYear(this.year)) {
                    if (this.day<29){
                        day++;
                    } else {
                        day=1;
                        this.month=3;
                    }
                } else {
                    if (this.day<28){
                        day++;
                    } else {
                        day=1;
                        this.month=3;
                    }
                }
                break;
            case 12:
                if (this.day<31){
                    day++;
                } else {
                    day=1;
                    this.month=1;
                    this.year++;
                }
                break;
        }
        System.out.printf("Следующая дата: %02d.%02d.%04d", this.day, this.month, this.year);
    }
    private boolean scan(String str) {
        StringBuilder dd = new StringBuilder();
        StringBuilder mm = new StringBuilder();
        StringBuilder yyyy = new StringBuilder();

        if ((str.length() == 10) && (str.charAt(2) == '.') && (str.charAt(5) == '.')) {
            try {
                this.day = day(str);
                this.month = month(str);
                this.year = year(str);
                if (year >=0 && month>=1 && month <=12) {
                    switch (month) {
                        case 1:
                        case 3:
                        case 5:
                        case 7:
                        case 10:
                        case 12:
                            if (day <= 31 && day >= 0) {
                                return true;
                            } else {
                                return false;
                            }
                        case 2:
                            if (leapYear(year) && day <=29 && day >=0) {
                                return true;
                            } else {
                                if (!leapYear(year) && day <= 28 && day >= 0) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        default:
                            if (day <= 30 && day >= 0) {
                                return true;
                            } else {
                                return false;
                            }
                        }

                    } else {
                    return false;
                }
                } catch(NumberFormatException ex){
                    return false;
                }
            } else {
                return false;
            }

        }

    private int day(String str) {
        StringBuilder dd = new StringBuilder();
        dd.append(str.charAt(0)).append(str.charAt(1));
        return Integer.parseInt(dd.toString());
    }

    private int month(String str) {
        StringBuilder mm = new StringBuilder();
        mm.append(str.charAt(3)).append(str.charAt(4));
        return Integer.parseInt(mm.toString());
    }

    private int year(String str) {
        StringBuilder yyyy = new StringBuilder();
        yyyy.append(str.charAt(6)).append(str.charAt(7)).append(str.charAt(8)).append(str.charAt(9));
        return Integer.parseInt(yyyy.toString());
    }


    private boolean leapYear(int year){
        boolean leap;
        if (year>=8 && year <1700 && year%4==0) {
            leap = true;
        } else {
            if (year >= 1700 && year % 4 == 0) {
                if (year%100==0) {
                    if (year%400==0) {
                        leap = true;
                    } else {
                        leap = false;
                    }
                } else {
                    leap = true;
                }
            } else {
                leap = false;
            }
        }
    return leap;
    }
}
