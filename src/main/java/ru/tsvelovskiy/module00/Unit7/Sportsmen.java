package main.java.ru.tsvelovskiy.module00.Unit7;

public class Sportsmen {
    Sportsmen(){
        System.out.printf("За 10 дней: %.2f км\n",tenDays());
        System.out.printf("100 км за %d дней\n",day100km());
        System.out.printf("20 км за день пробежит на %d день\n",dayOver20km());
    }
    public double tenDays(){
        return kmPerDays(10);
    }
    public double kmPerDays(int day) {
        double oldDay=10;
        double newDay=10;
        double dayKm=oldDay;
        for (int i=2; i<=day; i++){
            newDay=oldDay*1.1;
            dayKm+=newDay;
            oldDay=newDay;
        }
        return dayKm;
    }
    public int day100km(){
        int i=0;
         do {
            i++;
        } while (kmPerDays(i)<=100);

        return i;
    }
    public int dayOver20km(){
        double oldDay=10;
        double newDay=10;
        double dayKm=oldDay;
        int counter = 1;
        while (newDay<=20){
            newDay=oldDay*1.1;
            oldDay=newDay;
            counter++;
        }
        return counter;
    }
}
