package main.java.ru.tsvelovskiy.module01.unit1_2;

import java.util.Scanner;

public class Square {
    public Square(){
        this.square();
    }

    public void square() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите размер квадрата: ");
        int size = sc.nextInt();
        String[][] square = new String[size][size];
        for (int i=0; i<size; i++){
            for (int j=0; j<size; j++) {
                if (i>j) {
                    if (i+j<size-1){
                        square[i][j] = "Y";
                    } else if (i+j>=size){
                        square[i][j] = "T";
                    } else {
                        square[i][j] = "0";
                    }
                } else if (i<j) {
                    if (i + j <= size - 2) {
                        square[i][j] = "X";
                    } else if (i + j >= size) {
                        square[i][j] = "Z";
                    } else {
                        square[i][j] = "0";
                    }
                } else {
                    square[i][j] = "0";
                }
                System.out.printf("%s ",square[i][j]);
            }
            System.out.println();
        }

        System.out.println();
        for (int i=0; i<size; i++) {
            for (int j = 0; j < size; j++) {
                if (i!=(int)size/2 && j!=(int)size/2) {
                    System.out.printf("%s ",square[i][j]);
                }
            }
            if (i!=(int)size/2) {
                System.out.println();
            }
        }
    }
}
