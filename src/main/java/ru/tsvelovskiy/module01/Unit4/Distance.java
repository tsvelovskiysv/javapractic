package main.java.ru.tsvelovskiy.module01.Unit4;

import java.util.Scanner;

public class Distance {
    Distance(){
        double distance;
        int x1,x2,y1,y2;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите координаты точек: ");
        System.out.println("Введите x1:");
        x1 = sc.nextInt();
        System.out.println("Введите y1:");
        y1 = sc.nextInt();
        System.out.println("Введите x2:");
        x2 = sc.nextInt();
        System.out.println("Введите y2:");
        y2 = sc.nextInt();
        distance = distance(x1,y1,x2,y2);
        System.out.printf("Дистанция между точками %f: ", distance);


    }
    public double distance(int x1,int y1, int x2, int y2) {
        double distance=0;
        distance = Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
        return distance;
    }
}
