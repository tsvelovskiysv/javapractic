package main.java.ru.tsvelovskiy.module01.unit3;

import java.util.Scanner;

public class SquareEquation {
    SquareEquation(){
        int a,b,c;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите а:");
        a = sc.nextInt();
        System.out.println("Введите b:");
        b = sc.nextInt();
        System.out.println("Введите c:");
        c = sc.nextInt();
        this.squareEquation(a,b,c);
    }

    public void squareEquation(int a, int b, int c){
        int D = b*b-4*a*c;
        System.out.printf("D: %d\n", D);
        double x1=0;
        double x2=0;
        if (D<0) {
            System.out.print("Корней нет");
        } else {
            x1 = (-b+Math.sqrt(D))/2*a;
            System.out.printf("Первый корень: %.2f\n", x1);
            if (D>0) {
                x2 = (-b - Math.sqrt(D)) / 2 * a;
                System.out.printf("Второй корень: %.2f", x2);
            }
        }
    }
}
